# OSMSSurvey
Survey of genomics capabilities within OSMS. The purpose of this survey is to:

  1. to understand where there are common processes across OSMS genomics research  
  2. to understand the computational and analytical challenges faced by OSMS genomics  
  3. to help design and build data products and pipelines to overcome these challenges  
  4. to help foster and build core computational and analytical skills across OSMS to facilitate reproducible, scalable and efficient genomics research  

Initially, this survey is a means to understand the genomics landscape within OSMS. In particular, we are interested in identifying core processes and challenges faced across OSMS teams. This will then provide a roadmap to further discuss and define core challenges and thus, prioritise projects aimed at addressing these key challenges.

### Approximate Timeline  

26 Feb, 2016: draft survey out for feedback  
4 March, 2016: edit according to feedback & final survey ready for release  
7 March, 2016: survey sent out to participants  
11 March, 2016: follow up with participants on progress  
18 March, 2016: initial results of survey produced  
31 March, 2016: aimed to have met with all participants and discuss individually  
31 March, 2016: core processes and challenges clearly identified and prioritised.  



